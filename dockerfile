# First stage: complete build environment
FROM maven:3.8.4-jdk-17 AS builder

# add build files
COPY pom.xml ./
COPY src/ src/

# build application
RUN mvn clean package -DskipTests

# Second stage: minimal runtime environment
FROM openjdk:17-jdk-alpine

# copy jar from the first stage
COPY --from=builder target/demo-0.0.1-SNAPSHOT.jar /app.jar
CMD ["java", "-jar", "/app.jar"]
